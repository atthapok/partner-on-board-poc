import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InputExcelFileComponent } from './components/input-excel-file/input-excel-file.component';
import { PartnerOnboardComponent } from './pages/partner-onboard/partner-onboard.component';

@NgModule({
  declarations: [
    AppComponent,
    InputExcelFileComponent,
    PartnerOnboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

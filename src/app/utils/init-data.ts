const getOrSetValue = (obj, defaultValue) => {
  return obj ? obj.v : defaultValue;
}

export {
  getOrSetValue
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputExcelFileComponent } from './input-excel-file.component';

describe('InputExcelFileComponent', () => {
  let component: InputExcelFileComponent;
  let fixture: ComponentFixture<InputExcelFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputExcelFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputExcelFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';
import * as XLSX from 'xlsx'

@Component({
  selector: 'app-input-excel-file',
  templateUrl: './input-excel-file.component.html',
  styleUrls: ['./input-excel-file.component.css']
})

export class InputExcelFileComponent implements OnInit {

  @Input() onProcessFileChange: (workbook: any) => void;

  fileName = ""

  constructor() { }

  ngOnInit() {
  }

  onFileSelected = async (event) => {
    const file: File = event.target.files[0];
    if (!file) {
      this.onProcessFileChange(null)
    }
    this.fileName = file.name;
    await this.processExcelFile(file);
  }

  processExcelFile = async (file: File) => {
    const arrayBuffer = await this.readFile(file);
    const workbook = this.convertArrayBufferToWorkbook(arrayBuffer);
    this.onProcessFileChange(workbook)
  }

  readFile = (file: File) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = res => {
        resolve(reader.result);
      };
      reader.onerror = err => reject(err);
      reader.readAsArrayBuffer(file);
    });
  }

  convertArrayBufferToWorkbook = (arrayBuffer) => {
    const data = new Uint8Array(arrayBuffer);
    const arr = new Array();
    for (let i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
    const bstr = arr.join("");
    return XLSX.read(bstr, { type: "binary" });
  }
}

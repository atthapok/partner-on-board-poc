import { Component, OnInit } from '@angular/core';

import * as SHEET_NAME from '../../constants/sheet-name'
import mappingCoverSheetToJSON from '../../mappings/cover-mapping'

@Component({
  selector: 'app-partner-onboard',
  templateUrl: './partner-onboard.component.html',
  styleUrls: ['./partner-onboard.component.css']
})
export class PartnerOnboardComponent implements OnInit {

  jsonPreview = null

  constructor() { }

  ngOnInit() {
  }

  onProcessExcelFileChange = (workbook) => {
    if (!workbook) {
      console.log("Error reading file!");
      return;
    }
    const coverSheet = workbook.Sheets[SHEET_NAME.COVER_SHEET_NAME];
    const directCreditAPISheet = workbook.Sheets[SHEET_NAME.DIRECT_CREDIT_API_SHEET_NAME];
    const techInfoSheet = workbook.Sheets[SHEET_NAME.TECH_INFO_SHEET_NAME];
    const coverData = mappingCoverSheetToJSON(coverSheet);
    this.jsonPreview = JSON.stringify(coverData, null, 2);
  }

}

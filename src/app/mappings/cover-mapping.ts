import * as COVER_SHEET from '../constants/cover-sheet'
import { getOrSetValue } from '../utils/init-data'

const mappingCoverSheetToJSON = (coverSheet) => {
  const nameTh = getOrSetValue(coverSheet[COVER_SHEET.PARTNER_NAME_TH_CELL], "");
  const nameEn = getOrSetValue(coverSheet[COVER_SHEET.PARTNER_NAME_EN_CELL], "");
  const identificationNumber = getOrSetValue(coverSheet[COVER_SHEET.PARTNER_IDENTIFICATION_NUMBER_CELL], "");
  const typeOfIdentification = COVER_SHEET.PARTNER_TYPE_OF_IDENTIFICATION
  const billingAddress = {
    addressType: COVER_SHEET.BILLING_ADDRESS_TYPE,
    buildingName: getOrSetValue(coverSheet[COVER_SHEET.BILLING_BUILDING_NAME_CELL], ""),
    buildingNumber: getOrSetValue(coverSheet[COVER_SHEET.BILLING_BUILDING_NUMBER_CELL], ""),
    villageName: getOrSetValue(coverSheet[COVER_SHEET.BILLING_VILLAGE_NAME_CELL], ""),
    villageNumber: getOrSetValue(coverSheet[COVER_SHEET.BILLING_VILLAGE_NUMBER_CELL], ""),
    streetName: getOrSetValue(coverSheet[COVER_SHEET.BILLING_STREET_NAME_CELL], ""),
    lane: getOrSetValue(coverSheet[COVER_SHEET.BILLING_LANE_CELL], ""),
    subDistrict: getOrSetValue(coverSheet[COVER_SHEET.BILLING_SUB_DISTRICT_CELL], ""),
    district: getOrSetValue(coverSheet[COVER_SHEET.BILLING_DISTRICT_CELL], ""),
    province: getOrSetValue(coverSheet[COVER_SHEET.BILLING_STREET_NAME_CELL], ""),
    countryCode: getOrSetValue(coverSheet[COVER_SHEET.BILLING_PROVINCE_CELL], ""),
    postCode: getOrSetValue(coverSheet[COVER_SHEET.BILLING_POST_CODE_CELL], ""),
  }
  const currentAddress = {
    addressType: COVER_SHEET.CURRENT_ADDRESS_TYPE,
    buildingName: getOrSetValue(coverSheet[COVER_SHEET.CURRENT_BUILDING_NAME_CELL], ""),
    buildingNumber: getOrSetValue(coverSheet[COVER_SHEET.CURRENT_BUILDING_NUMBER_CELL], ""),
    villageName: getOrSetValue(coverSheet[COVER_SHEET.CURRENT_VILLAGE_NAME_CELL], ""),
    villageNumber: getOrSetValue(coverSheet[COVER_SHEET.CURRENT_VILLAGE_NUMBER_CELL], ""),
    streetName: getOrSetValue(coverSheet[COVER_SHEET.CURRENT_STREET_NAME_CELL], ""),
    lane: getOrSetValue(coverSheet[COVER_SHEET.CURRENT_LANE_CELL], ""),
    subDistrict: getOrSetValue(coverSheet[COVER_SHEET.CURRENT_SUB_DISTRICT_CELL], ""),
    district: getOrSetValue(coverSheet[COVER_SHEET.CURRENT_DISTRICT_CELL], ""),
    province: getOrSetValue(coverSheet[COVER_SHEET.CURRENT_STREET_NAME_CELL], ""),
    countryCode: getOrSetValue(coverSheet[COVER_SHEET.CURRENT_PROVINCE_CELL], ""),
    postCode: getOrSetValue(coverSheet[COVER_SHEET.CURRENT_POST_CODE_CELL], ""),
  }
  const partnerRepresentative = {
    firstName: getOrSetValue(coverSheet[COVER_SHEET.PARTNER_REPRESENTATIVE_FIRST_NAME_CELL], ""),
    lastName: getOrSetValue(coverSheet[COVER_SHEET.PARTNER_REPRESENTATIVE_LAST_NAME_CELL], ""),
    mobile: getOrSetValue(coverSheet[COVER_SHEET.PARTNER_REPRESENTATIVE_CONTACT_MOBILE_CELL], ""),
    email: getOrSetValue(coverSheet[COVER_SHEET.PARTNER_REPRESENTATIVE_CONTACT_EMAIL_CELL], ""),
  }
  const partnerSupport = {
    firstName: getOrSetValue(coverSheet[COVER_SHEET.PARTNER_REPRESENTATIVE_FIRST_NAME_CELL], ""),
    lastName: getOrSetValue(coverSheet[COVER_SHEET.PARTNER_REPRESENTATIVE_LAST_NAME_CELL], ""),
    mobile: getOrSetValue(coverSheet[COVER_SHEET.PARTNER_REPRESENTATIVE_CONTACT_MOBILE_CELL], ""),
    email: getOrSetValue(coverSheet[COVER_SHEET.PARTNER_REPRESENTATIVE_CONTACT_EMAIL_CELL], ""),
  }

  return {
    nameTh,
    nameEn,
    identificationNumber,
    typeOfIdentification,
    billingAddress,
    currentAddress,
    partnerRepresentative,
    partnerSupport
  }
}

export default mappingCoverSheetToJSON
